use std::fs;

pub fn part1() {
    let file = fs::read_to_string("./inputs/day2");
    if file.is_err() {
        println!("Couldn't open the file!");
        return;
    }

    let input = file.unwrap();

    let lines = input.lines().collect::<Vec<_>>();

    let mut number_of_valid_passwords: i32 = 0;

    for line in lines {
        let mut split = line.split(' ');

        let rule = split.next().unwrap();
        let mut rule_split = rule.split('-');
        let minimum = rule_split.next().unwrap().parse::<i32>().unwrap();
        let maximum = rule_split.next().unwrap().parse::<i32>().unwrap();

        let character = split.next().unwrap().chars().next().unwrap();

        let password = split.next().unwrap();
        let count_of_character = password.matches(character).count();
        if count_of_character >= minimum as usize && count_of_character <= maximum as usize {
            number_of_valid_passwords += 1;
        }
    }
    println!("day 2, part 1:");
    println!("{} valid passwords", number_of_valid_passwords);
}

pub fn part2() {
    let file = fs::read_to_string("./inputs/day2");
    if file.is_err() {
        println!("Couldn't open the file!");
        return;
    }

    let input = file.unwrap();

    let lines = input.lines().collect::<Vec<_>>();

    let mut number_of_valid_passwords: i32 = 0;

    for line in lines {
        let mut split = line.split(' ');

        let rule = split.next().unwrap();
        let mut rule_split = rule.split('-');
        let first = (rule_split.next().unwrap().parse::<i32>().unwrap() - 1) as usize;
        let second = (rule_split.next().unwrap().parse::<i32>().unwrap() - 1) as usize;

        let character = split.next().unwrap().chars().next().unwrap();

        let password = split.next().unwrap();

        let character_list = password.chars().collect::<Vec<_>>();
        let mut is_correct = false;
        if character_list[first] == character {
            is_correct = true;
        }
        if character_list[second] == character {
            if !is_correct {
                number_of_valid_passwords += 1;
            }
        } else {
            if is_correct {
                number_of_valid_passwords += 1;
            }
        }
    }
    println!("day 2, part 2:");
    println!("{} valid passwords", number_of_valid_passwords);
}
